package main

import (
	"flag"
	"fmt"
	"io"
	"net/http"
	"os"

	"gitlab.com/searsaw/proxy"
	yaml "gopkg.in/yaml.v2"
)

func parseSites(filepath string) ([]proxy.Site, error) {
	fd, err := os.Open(filepath)
	if err != nil {
		return nil, err
	}

	var config proxy.Config
	err = yaml.NewDecoder(fd).Decode(&config)
	if err != nil {
		return nil, err
	}

	return config.Sites, nil
}

func handleError(w http.ResponseWriter, status int, body string) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	w.Write([]byte(body))
}

func siteHandler(site proxy.Site) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		path := r.URL.Path[len(site.Path):]
		url := fmt.Sprintf("http://%s:%d%s", site.Service, site.Port, path)
		serviceReq, err := http.NewRequest(r.Method, url, r.Body)
		if err != nil {
			body := `{"code": 500, "msg": "Something went wrong when creating the proxied request"}`
			handleError(w, http.StatusInternalServerError, body)
			return
		}

		serviceRes, err := http.DefaultClient.Do(serviceReq)
		if err != nil {
			body := `{"code": 500, "msg": "Something went wrong when receiving a response from the %s"}`
			handleError(w, http.StatusInternalServerError, fmt.Sprintf(body, site.Service))
			return
		}
		defer serviceRes.Body.Close()

		w.WriteHeader(serviceRes.StatusCode)
		io.Copy(w, serviceRes.Body)
	}
}

func main() {
	sitesConfig := flag.String("sites", "sites.yml", "the path to the configuration for sites")
	flag.Parse()

	sites, err := parseSites(*sitesConfig)
	if err != nil {
		panic(err)
	}

	for _, site := range sites {
		http.HandleFunc(site.Path, siteHandler(site))
	}

	fmt.Println("The proxy is running.")
	http.ListenAndServe(":8080", nil)
}
