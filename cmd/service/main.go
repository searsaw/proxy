package main

import (
	"fmt"
	"net/http"
	"os"
)

func wrapHandlers(f http.HandlerFunc) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")

		if r.Method != http.MethodGet {
			w.WriteHeader(http.StatusMethodNotAllowed)
			w.Write([]byte(`{"code": 405, "msg": "Method not allowed"}`))
			return
		}

		f(w, r)
	}
}

func healthcheck(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	w.Write([]byte(fmt.Sprintf(`{"hostname": "%s", "code": 200, "msg": "healthcheck passed"}`, os.Getenv("SERVICE_NAME"))))
}

func endpoint(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	w.Write([]byte(fmt.Sprintf(`{"hostname": "%s"}`, os.Getenv("SERVICE_NAME"))))
}

func main() {
	http.HandleFunc("/healthcheck", wrapHandlers(healthcheck))
	http.HandleFunc("/endpoint", wrapHandlers(endpoint))

	fmt.Println("The server is running.")
	http.ListenAndServe(":8080", nil)
}
